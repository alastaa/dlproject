import os
import numpy as np
import json

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from modules import VqVae


PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


def main():
    device = torch.device('cpu')
    transform = transforms.Compose([
        transforms.ToTensor(),  # Transform to tensor
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    data_dir = os.path.join(PROJECT_DIR, 'data', 'CIFAR10')
    trainset = torchvision.datasets.CIFAR10(root=data_dir, train=True, download=True, transform=transform)
    testset = torchvision.datasets.CIFAR10(root=data_dir, train=False, download=True, transform=transform)

    trainloader = torch.utils.data.DataLoader(trainset, batch_size=32, shuffle=True)
    testloader = torch.utils.data.DataLoader(testset, batch_size=5, shuffle=False)


    models = [
        VqVae([512], [1]),
        VqVae([512,512], [1,1]),
        VqVae([512]*4, [1]*4),
        VqVae([256]*4, [1]*4),
        VqVae([256]*8, [1]*8),
        VqVae([128]*16, [1]*16),
        VqVae([128]*8, [1]*8),
        VqVae([64]*32, [1]*32),
        VqVae([64]*64, [1]*64),
        VqVae([32]*64, [1]*64),
        VqVae([512,256,256,128,128,128,128], [8,2,2,1,1,1,1]),
        VqVae([512,10,10], [62,1,1]),
        VqVae([256,256,256,256,16,16,16], [12,12,12,12,2,2,2]),
        VqVae([512,256,128,64,28,14,8,4],[1]*8),
        VqVae([512,256,128,64,28,14,8,4],[32,16,8,4,1,1,1,1]),
        VqVae([512,256,256,128,10],[32,16,8,4,4]),
        VqVae([512,16,16],[62,1,1]),
        VqVae([512,32,32],[62,1,1]),
        VqVae([512,64],[62,2]),
        VqVae([512,16,16,16,16],[60,1,1,1,1]),
        VqVae([512,16],[63,1]),
        VqVae([512,32],[63,1]),
    ]

    names = [
        "VqVae([512],[1])",
        "VqVae([512,512],[1,1])",
        "VqVae([512]*4,[1]*4)",
        "VqVae([256]*4,[1]*4)",
        "VqVae([256]*8,[1]*8)",
        "VqVae([128]*16,[1]*16)",
        "VqVae([128]*8,[1]*8)",
        "VqVae([64]*32,[1]*32)",
        "VqVae([64]*64,[1]*64)",
        "VqVae([32]*64,[1]*64)",
        "VqVae([512,256,256,128,128,128,128],[8,2,2,1,1,1,1]*8)",
        "VqVae([512,10,10],[62,1,1])",
        "VqVae([256,256,256,256,16,16,16],[12,12,12,12,2,2,2])",
        "VqVae([512,256,128,64,28,14,8,4],[1]*8)",
        "VqVae([512,256,128,64,28,14,8,4],[32,16,8,4,1,1,1,1])",
        "VqVae([512,256,256,128,10],[32,16,8,4,4])",
        "VqVae([512,16,16],[62,1,1])",
        "VqVae([512,32,32],[62,1,1])",
        "VqVae([512,64],[62,2])",
        "VqVae([512,16,16,16,16],[60,1,1,1,1])",
        "VqVae([512,16],[63,1])",
        "VqVae([512,32],[63,1])",
    ]

    for model, name in zip(models,names):
        try:
            print("--------- {} --------------".format(name))
            trained_model = train_model(model, name, trainloader,
                                        testloader, device)
            print("-------------------------------------------")
        except Exception as e:
            print(e)


def train_model(model, name, trainloader, testloader, device):
    criterion = nn.MSELoss()
    parameters = list(model.parameters())
    optimizer = optim.Adam(parameters, lr=0.001)
    logs = []
    for epoch in range(4):
        running_loss = 0.0
        for i, (inputs, labels) in enumerate(trainloader, 0):
            inputs = inputs.to(device)
            optimizer.zero_grad()
            outputs, _, _ = model(inputs)
            loss = criterion(outputs, inputs)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            logs.append([epoch, i, running_loss])
            if i % 200 == 199:    # print every 200 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch+1, i+1, running_loss/200))
                running_loss = 0.0
    print('Finished Training')
    log_path = os.path.join(PROJECT_DIR, 'logs', name + '.json')
    with open(log_path, 'w') as jf:
        jf.write(json.dumps(logs))

    model_path = os.path.join(PROJECT_DIR, 'models', name + '.pickle')
    torch.save(model, model_path)

    with torch.no_grad():
        model.eval()
        test_loss = 0.0
        for inputs, labels in testloader:
            outputs,_,_ = model(inputs)
            loss = criterion(outputs, inputs)
            test_loss += loss.item()
        print('Test loss:', test_loss)
        with open(os.path.join(PROJECT_DIR,'logs','test.log'), 'a+') as lf:
            lf.write('{} test loss: {}\n'.format(name,test_loss))

        testiter = iter(testloader)
        test_lst = []
        for i in range(5):
            batch, _ = testiter.next()
            test_lst.append(batch)
        inputs = torch.cat(test_lst,0)
        reconstructions, _, _ = model(inputs[:-1,:,:,:])
        image_name = name + ".png"
        image_path = os.path.join(PROJECT_DIR, 'images', image_name)
        torchvision.utils.save_image(reconstructions, image_path, normalize=True)


if __name__ == "__main__":
    main()
