
import torch

import torch.nn as nn
import torch.nn.functional as F

from nearest_embed import NearestEmbed


class ResBlock(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.block = nn.Sequential(
            nn.ReLU(True),
            nn.Conv2d(dim, dim, 3, 1, 1),
            nn.BatchNorm2d(dim),
            nn.ReLU(True),
            nn.Conv2d(dim, dim, 1),
            nn.BatchNorm2d(dim)
        )

    def forward(self, x):
        return x + self.block(x)


class VqVae(nn.Module):
    """ k_list                  -> list of embedding space sizes

        emb_space_proportions   -> how many latent vectors belong to each
                                   embedding space

        for example k_list = [256,256] and emb_space_proportions = [1,1]
        means two embedding spaces with 256 vectors and latent vectors are split
        evenly
        """


    def __init__(self, k_list, emb_space_proportions, input_dim=3, dim=20):
        super(VqVae, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(input_dim, dim, 4, 2, 1),
            nn.BatchNorm2d(dim),
            nn.ReLU(True),
            nn.Conv2d(dim, dim, 4, 2, 1),
            ResBlock(dim),
            nn.BatchNorm2d(dim),
            ResBlock(dim),
            nn.BatchNorm2d(dim),
        )
        self.k_list = k_list
        self.emb_space_proportions = emb_space_proportions
        self.proportions_sum = sum(self.emb_space_proportions)
        self.embeddings = [NearestEmbed(self.k_list[i], dim) for i in range(len(self.k_list))]
        self.decoder = nn.Sequential(
            ResBlock(dim),
            nn.BatchNorm2d(dim),
            ResBlock(dim),
            nn.ReLU(True),
            nn.ConvTranspose2d(dim, dim, 4, 2, 1),
            nn.BatchNorm2d(dim),
            nn.ReLU(True),
            nn.ConvTranspose2d(dim, input_dim, 4, 2, 1),
            nn.Tanh()
        )

    def encode(self,x):
        x = self.encoder(x)
        return x

    def decode(self, x):
        x = self.decoder(x)
        return x

    def encoder_output_to_embeddings_list(self, z_e, latent_shape):
        B, D, H, W = latent_shape
        z_e = z_e.view(B, D, -1)
        z_e_ls = []
        index_sum = 0
        for i in range(len(self.k_list)):
            index = int(64*(self.emb_space_proportions[i]/self.proportions_sum))
            z_e_ls.append(z_e[:, :, index_sum:index_sum+index])
            index_sum += index

        z_q_ls = []
        emb_ls = []
        for i in range(len(self.embeddings)):
            z_q, _ = self.embeddings[i](z_e_ls[i])
            z_q_ls.append(z_q)
            emb, _ = self.embeddings[i](z_e_ls[i].detach())
            emb_ls.append(emb)
        return z_q_ls, emb_ls

    def embeddings_list_to_decoder_input(self, z_q_ls, latent_shape):
        B, D, H, W = latent_shape
        z_q = torch.cat(z_q_ls, dim=2)
        z_q = z_q.view(B, D, H, W)
        return z_q

    def forward(self, x):
        z_e = self.encode(x)
        latent_shape = z_e.shape
        z_q_ls, emb_ls = self.encoder_output_to_embeddings_list(z_e, latent_shape)
        z_q = self.embeddings_list_to_decoder_input(z_q_ls, latent_shape)
        return self.decode(z_q), z_e, emb_ls
